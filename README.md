##项目介绍 考研小程序


#### start

1. git init 
2. git remote add origin https://gitee.com/KID0323/postgraduate_program.git
3. git pull origin master


#### 目录说明

**assert**

项目资源文件夹，包含图片资源，ui框架，以及依赖的第三方插件等等

> 受小程序项目大小的限制，一些大文件（例如图片）不要放在本地

**components**

小程序组件文件夹，存放开发过程中自定义的组件

**config**

config文件夹存放开发过程中的一些常用的配置选项

    - api.js 开发接口文档
    - errcodes.js 服务器错误代码说明
    - index.js 常用配置，例如服务器地址等等
    
**pages**

**comments** 

公共组件

**utils**

常用工具资源文件夹