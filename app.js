//app.js
const { SERVICETEL } = require('config/index.js');
const Towxml = require('/assert/towxml/main.js');  //  wowxml把HTML转换为wxml
App({
	onLaunch: function () {

	},	
	makePhoneCall: function () {
		wx.makePhoneCall({
			phoneNumber: SERVICETEL, //仅为示例，并非真实的电话号码
			success: function (e) {
				console.log(e)
			},
			fail: function () {
				console.log('无此电话')
			}
		})
	},
	towxml: new Towxml(),  // 添加为全局的对象，这样每个页面都可以用到
})