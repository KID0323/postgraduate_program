Component({
	properties: {
		course: {
			type: Array,
			value: []
		},
		"general": {
			type: Boolean,
			value: true
		},
	},
	data: {
	
	},
	methods: {
		showDetail(e){
			this.triggerEvent('showDetail',e.currentTarget.dataset.id)
		}
	}
})
