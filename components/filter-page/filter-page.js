Component({
	properties: {
		currfilter: {
			type: Object,
			value: {}
		},
		matches: {
			type: Array,
			value: []
		},
		"placeholder": {
			type: String,
			value: ''
		},
		"general": {
			type: Boolean,
			value: true
		},
		
	},
	data: {
		isloading: true,
		inputShowed: false,
		selected: {},  // 当前选中筛选项
		menuTitle: {},  // 当前筛选项显示
		menuState: false,  // 菜单状态默认收起
		currentMenu: {},  // 当前选择的下拉菜单
		currentall: { 'school': true, 'project': true, 'type': true}
	},
	ready(){
		let selected = this.data.selected;
		if (!this.properties.currfilter.type ){  // 监测课程列表初次进入是否带有筛选项
			return false;
		}
		let key = this.properties.currfilter.type || ''
		selected[key] = this.properties.currfilter.id || ''
		this.setData({
			selected,
		})
	},
	methods: {
		menuDown(e){
			this.setData({
				currentMenu: e.currentTarget.dataset.type || this.data.currentMenu,  // 菜单类型
				menuState:true,
			})
			this.triggerEvent('menuDown', this.data.currentMenu)
		},
		menuClose(){
			this.setData({
				menuState: false,
			})
			this.triggerEvent('current')		
		},
		current(e){
			let typeid = e.currentTarget.dataset.typeid; // 筛选类型
			let val = e.currentTarget.dataset.val;
			let selected = this.data.selected;
			let menuTitle = this.data.menuTitle;
			let all = e.currentTarget.dataset.alltype;
			let currentall = this.data.currentall;

			typeid == "-1" ? currentall[all] = true : currentall[all] = false;
			typeid == "-1" ? selected[this.data.currentMenu] = '' : selected[this.data.currentMenu] = typeid;
			menuTitle[this.data.currentMenu] = (val == -1) ? '' : val
			
			this.setData({
				isloading: false,
				menuState: false,
				menuTitle,
				selected,
				currentall
			})
			this.triggerEvent('current', this.data.selected)		
		},

		showInput(e) {
			this.setData({
				inputShowed: true
			});
		},
		hideInput(e) {
			this.setData({
				//inputVal: "",
				inputShowed: false
			});
		},
		clearInput(e) {
			this.setData({
				inputVal: ""
			});
		},
		inputTyping(e) {
			this.setData({
				inputVal: e.detail.value
			});
		},
		search(e) {
			this.setData({
				inputShowed: false,
				currentMenu: {},  // 当前选择的下拉菜单
				menuTitle: {},
				selected: {},
				currentall: { 'school': true, 'project': true, 'type': true }
			})
			this.triggerEvent('search', this.data.inputVal);
		},
		onResetSearch(e) {
			this.setData({
				inputVal: ""
			});
			this.triggerEvent('search', this.data.inputVal);
		},
		cancelFilter(e) {
			this.setData({
				menuState: false
			})
			this.triggerEvent('onmask')
		},
	}
})
