const { BASEAPIURL } = require('../index.js')
const { wxRequest } = require('../../utils/request.js')

const courseList = function (param) {
	return wxRequest('/jianzhangs', param)
}

module.exports = {
	courseList  // 推荐课程
}