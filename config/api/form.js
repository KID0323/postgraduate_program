const { BASEAPIURL } = require('../index.js')
const { wxRequest } = require('../../utils/request.js')

const gbook = function (param) {
	return wxRequest('/gbook', param, 'post')
}
module.exports = {
	gbook,  // 有问必答 预约报名
}