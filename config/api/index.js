const crouse = require('./crouse.js')
const project = require('./project.js')
const promotion = require('./promotion.js')
const teacher = require('./teacher.js')
const school = require('./school.js')
const form = require('./form.js')

module.exports = {
	...form,
	...crouse,
	...project,
	...promotion,
	...teacher,
	...school
}