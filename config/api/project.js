const { BASEAPIURL } = require('../index.js')
const { wxRequest } = require('../../utils/request.js')

const projectList = function (param) {
	return wxRequest('/projects', param)
}

module.exports = {
	projectList  // 科目分类
}