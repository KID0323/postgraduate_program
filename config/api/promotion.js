const { BASEAPIURL } = require('../index.js')
const { wxRequest } = require('../../utils/request.js')

const spList = function (param) {
	return wxRequest('/promotions', param)
}

module.exports = {
	spList  // 活动列表
}