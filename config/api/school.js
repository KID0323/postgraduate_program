const { BASEAPIURL } = require('../index.js')
const { wxRequest } = require('../../utils/request.js')

const schoolList = function (param) {
	return wxRequest('/schools', param)
}

module.exports = {
	schoolList  // 机构列表
}