const { BASEAPIURL } = require('../index.js')
const { wxRequest } = require('../../utils/request.js')

const teacherList = function (param) {
	return wxRequest('/teachers', param)
}

module.exports = {
	teacherList  // 老师列表
}