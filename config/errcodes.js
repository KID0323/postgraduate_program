module.exports = {
	'500':'param Error',
	'-41001': 'encodingAesKey 非法',
	'-41002': 'Iv 非法',
	'-41003': 'aes 解密失败',
	'-41004': '解密后得到的buffer非法',
	'-42001': 'jscode2session error',
	'-42002': 'session error; maybe no login',
	'-42003': 'Args Error',
	'-42004': 'Not Found',
	'-42005': 'Db Error'
}