// pages/details-page/details-page.js
const { spList } = require('../../config/api/index.js')
const { formatTime, diffTime } = require('../../utils/util.js')
const app = getApp()
let self

Page({

  /**
   * 页面的初始数据
   */
  data: {
		isloading: true,
		spLists: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
		self = this;
		spList({
			data:{
				id: options.id,
				category: 1,
				fields: 'content'
			},
			success(res) {

				try {
					let item = res.data
					item.yuloo_price = !item.yuloo_price ? item.yuloo_price : item.yuloo_price.split('.')[0]
					res.data.content = app.towxml.toJson(res.data.content);  // 获取详情
					item.start_time = (!parseInt(item.start_time) || item.start_time.indexOf("-") != -1) ? item.start_time : formatTime(new Date(parseInt(item.start_time)))
					
					item.end_time = (!parseInt(item.end_time) || item.end_time.indexOf("-") != -1) ? "永久" : formatTime(new Date(parseInt(item.end_time)))

					item.state = diffTime(item.start_time, false) ? (diffTime(item.end_time, false) ? "已结束" : "进行中") : "未开启"
				} finally {
					self.setData({
						spLists: res.data
					})
				}
			},
			complete() {
				self.setData({
					isloading: false,
				})
			}
		})
  },
	goHome(e){
		wx.navigateTo({
			url: '/pages/porject-page/porject-page?id=' + e.currentTarget.dataset.id,
		})
	},
	sign(e) {
		wx.navigateTo({
			url: '/pages/form-page/form-page?type=sp&title=活动报名&id=' + e.currentTarget.dataset.id,
		})
	},
	makePhoneCall: function () { // 拨号
		app.makePhoneCall()
	},
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})