// pages/course-detail/course-detail.js
const { schoolList, spList, courseList, projectList } = require('../../config/api/index.js')
const app = getApp();
let self
Page({

  /**
   * 页面的初始数据
   */
  data: {
		isloading: true,
		courseLists: [],
		spLists:[],
		discountShow: false,
		mask: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
		self = this;
		wx.showLoading({
			title: '加载中...',
			mask: true
		})

		courseList({
			data:{
				category: 1,
				id: options.id,
				fields: 'content'
			},
			// success(res) {
			// 	try{
			// 		res.data.content = app.towxml.toJson(res.data.content);  // 获取详情
			// 	} finally {
			// 		let item = res.data;
			// 		item.yuloo_price = !item.yuloo_price ? item.yuloo_price : item.yuloo_price.split('.')[0]
			// 		self.setData({
			// 			courseLists: res.data,
			// 		})
					
			// 	}
			// },
			// complete(res) {
			// 	self.setData({
			// 		isloading: false,
			// 	})
			// }
		}).then((res) => {
			try{
				res.data.content = app.towxml.toJson(res.data.content)
			} finally {
				let data = res.data;
				data.yuloo_price = !data.yuloo_price ? data.yuloo_price : data.yuloo_price.split('.')[0]
				self.setData({
					courseLists: res.data,
				})
			}
			spList({
				data: {
					category: 1,
					school: res.data.school_id,
					per_page: 1,
					type: 0,
					fields: 'smallText,content'
				},
				success(res) {
					if (res.data.items.length > 0) {
						try {
							res.data.items[0].content = app.towxml.toJson(res.data.items[0].content);  // 获取详情
						} finally {
							self.setData({
								spLists: res.data.items
							})
						}
					}
				},
				complete() {
					self.setData({
						isloading: false,
					})
				}
			})
		})
	
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },
	showDiscount(e) {
		self.setData({
			discountShow: true,
			mask: true
		})
	},
	hiddenDiscount(e) {
		self.setData({
			discountShow: false,
			mask: false
		})
	},
	makePhoneCall: function () { // 拨号
		app.makePhoneCall()
	},
	goHome: function () {
		wx.reLaunch({
			url: '/pages/tabbar/index/index'
		})
	},
	sign(e) {
		wx.navigateTo({
			url: '/pages/form-page/form-page?type=course&title=预约报名&id=' + e.currentTarget.dataset.id,
		})
	},
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})