// pages/porject-page/porject-page.js
const { schoolList, spList, courseList, teacherList } = require('../../config/api/index.js')
const { formatTime, diffTime } = require('../../utils/util.js')
const app = getApp()
let self

Page({

  /**
   * 页面的初始数据
   */
  data: {
		isloading: true,
		teacher: [],
		spList: [],
		schoollists:[],
		courseLists:[],
		general: true,
		schoolid: '',  // 机构id
		curPage: 1,  // 当前页
		perPage: 10,  // 每页的数量
		noMoreState: false,  // 没有更多数据
		moreCoursesLoading: false,  // 加载更多课程的状态,
		cntState: false,  // 内容显示状态
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
		self = this;
		wx.showLoading({
			title: '加载中...',
			mask: true
		})
		self.setData({
			schoolid: options.id
		})
		schoolList({ // 获取机构内容
			data: {
				id: options.id,
				category: 1,
				fields: 'content'
			},
			success(res) {
				try {
					res.data.content = app.towxml.toJson(res.data.content);  // 获取详情
				} finally {
					self.setData({
						schoollists: res.data
					})
				}
			},
			complete(res) {
				self.setData({
					isloading: false
				})
			}
		})
		teacherList({  // 获取机构教师
			data: {
				school: options.id,
				category: 1,
				per_page: 10,
			},
			success(res) {
				self.setData({
					teacher: res.data.items
				})
			},
			complete(res) {
				self.setData({
					isloading: false
				})
			}
		})
		spList({  // 获取机构活动
			data: {
				school: options.id,
				category: 1,
				per_page: 3,
			},
			success(res) {
				res.data.items.forEach((item) => {
					item.yuloo_price = !item.yuloo_price ? item.yuloo_price : item.yuloo_price.split('.')[0]

					item.start_time = (!parseInt(item.start_time) || item.start_time.indexOf("-") != -1) ? item.start_time : formatTime(new Date(parseInt(item.start_time)))
					item.end_time = (!parseInt(item.end_time) || item.end_time.indexOf("-") != -1) ? "永久" : formatTime(new Date(parseInt(item.end_time)))

					item.state = diffTime(item.start_time, false) ? (diffTime(item.end_time, false) ? "已结束" : "进行中") : "未开启"
				})
				self.setData({
					spList: res.data.items
				})
			},
			complete(res) {
				self.setData({
					isloading: false
				})
			}
		})
		self.getCourseList()
  },
	getCourseList(params) {  // 获取机构课程
		const { perPage, courseLists, schoolid } = self.data;
		return courseList({
			data: {
				...{
					school: schoolid,
					category: 1,
					per_page: 6,
				},
				...params
			},
			success(res) {
				let totalPages = res.data.total_pages;
				res.data.items.forEach((item) => {
					item.yuloo_price = !item.yuloo_price ? item.yuloo_price : item.yuloo_price.split('.')[0]
					item.start_time = (!parseInt(item.start_time) || item.start_time.indexOf("-") != -1) ? item.start_time : formatTime(new Date(parseInt(item.start_time)))

				})
				self.setData({
					courseLists: res.data.items,
				})
			},
			complete(res) {
				self.setData({
					isloading: false,
					moreCoursesLoading: false,  // 加载更多课程的状态
				})
			}
		})
	},
	cntShow: function (e){
		console.log(e)
		self.setData({
			cntState: true
		})
	},
	hideCnt(e) {
		self.setData({
			cntState: false
		})
	},
	navActlPage(e) {
		wx.navigateTo({
			url: '/pages/act-detail/act-detail?id=' + e.currentTarget.dataset.id,
		})
	},
	showDetail(e) {
		wx.navigateTo({
			url: '/pages/course-detail/course-detail?id=' + e.detail,
		})
	},
	goHome(e) {
		wx.reLaunch({
			url: '/pages/tabbar/index/index',
		})
	},
	sign(e) {
		wx.navigateTo({
			url: '/pages/form-page/form-page?type=school&title=机构-预约留言&id=' + e.currentTarget.dataset.id,
		})
	},
	
	makePhoneCall: function () { // 拨号
		app.makePhoneCall()
	},
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})