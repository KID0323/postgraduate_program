const { formatTime, dataInterval, diffTime } = require('../../../utils/util.js')
const { spList } = require('../../../config/api/index.js')
const app = getApp()
let self

Page({

  /**
   * 页面的初始数据
   */
  data: {
		isloading: true,
		splists: [],
		general: false,
		curPage: 1,  // 当前页
		perPage: 10,  // 每页的数量
		noMoreState: false,  // 没有更多数据
		schoolid: '',  // 机构id
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
		self = this;
		wx.showLoading({
			title: '加载中...',
		})
		self.setData({
			isloading: true,
			schoolid: options.id || ''
		})
		self.getSpList()

  },
	getSpList(params, extend){
		const { perPage, splists, schoolid } = self.data;
		return spList({
			data: {
				...{
					school: schoolid,
					category: 1,
					per_page: 10,
				},
				...params
			},
			success(res) {
				let totalPages = res.data.total_pages;
				res.data.items.forEach((item) => {
					item.yuloo_price = !item.yuloo_price ? item.yuloo_price : item.yuloo_price.split('.')[0]
				
					item.start_time = (!parseInt(item.start_time) || item.start_time.indexOf("-") != -1) ? item.start_time : formatTime(new Date(parseInt(item.start_time)))
					item.end_time = (!parseInt(item.end_time) || item.end_time.indexOf("-") != -1) ? "永久" : formatTime(new Date(parseInt(item.end_time)))
					
					item.state = diffTime(item.start_time, false) ? (diffTime(item.end_time, false) ? "已结束" : "进行中" ) : "未开启"
				})
				if (extend) {
					let curPage = Math.ceil((splists.length + res.data.items.length) / perPage);
					self.setData({
						splists: [...splists, ...res.data.items],
						curPage,
						noMoreState: parseInt(totalPages) <= curPage
					})
				} else {
					self.setData({
						splists: res.data.items,
						curPage: 1,
						noMoreState: parseInt(totalPages) <= Math.ceil(res.data.items.length / perPage)
					})
				}
			},
			complete(res) {
				self.setData({
					isloading: false,
					moreCoursesLoading: false,
				})
			}
		})
	},
  onReachBottom: function () {
		if (self.data.noMoreState) {
			return false;
		}
		self.setData({
			moreCoursesLoading: true,
		})
		self.getSpList({ page: self.data.curPage + 1, }, true)
  },
  showDetail(e){
		wx.navigateTo({
			url: '/pages/act-detail/act-detail?id=' + e.detail,
		})
	},
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})