const { formatTime } = require('../../../utils/util.js')
const { schoolList, spList, courseList } = require('../../../config/api/index.js')
const { setStorageSync, getStorageSync, removeStorage, clearStorageSync } = require('../../../utils/wcache.js')
const app = getApp()
let self
Page({
  data: {
		imgUrls: [
			{
				imgUrl: '/assert/imgs/banner1.jpg',
				swiperUrl: '/pages/porject-page/porject-page?id=253',
				navType: 'pages'
			},
			{
				imgUrl: '/assert/imgs/banner2.jpg',
				swiperUrl: '/pages/porject-page/porject-page?id=255',
				navType: 'pages'
			}
		],
		isloading: true,
    hasUserInfo: false,
		inputShowed: false,
		inputVal: "",
		spList: [],  // 活动列表
		schoolLists: [],  // 机构列表
		courseLists: [],  // 课程列表
		curPage: 1,  // 当前页
		perPage: 10,  // 每页的数量
		noMoreState: false,  // 没有更多数据
		openState: false,
		general: true,
		moreCoursesLoading: false,  // 加载更多课程的状态
		dTime: 3600,  // 本地存储有效时间3600s
  },
  onLoad: function () {
		self = this;
		wx.showLoading({
			title: '加载中...',
		})
		spList({
			data: {
				category: 1,
				per_page: 3,
			},
			success(res) {
				self.setData({
					spList: res.data.items
				})
			},
			complete(res){
				self.setData({
					isloading: false
				})
			}
		})
		self.getSchoolList();
		self.getCourseList()
  },
	onShow: function () {	
	},
	getSchoolList(params, extend) {  // 获取机构列表
		const { schoolLists, openState, dTime } = self.data;
		return schoolList({
			data:{
				...{
					category: 1,
					per_page: 9,
				},
				...params
			},
			success(res) {	
				self.setData({
					schoolLists: res.data.items
				})	
				if (openState){
					setStorageSync('ky_school', res.data.items, dTime)
				}
			},
			complete(res) {
				self.setData({
					isloading: false
				})
			}
		})
	},
	getCourseList(params) {
		const { perPage, courseLists } = self.data;
		return courseList({
			data:{
				...{
					category: 1,
					per_page: 6,
				},
				...params
			},
			success(res) {
				let totalPages = res.data.total_pages;
				res.data.items.forEach((item) => {
				
					item.yuloo_price = !item.yuloo_price ? item.yuloo_price : item.yuloo_price.split('.')[0] 
					item.start_time = (!parseInt(item.start_time) || item.start_time.indexOf("-") != -1) ? item.start_time : formatTime(new Date(parseInt(item.start_time)))

				})
				self.setData({
					courseLists: res.data.items,
				})
			},
			complete(res) {
				self.setData({
					isloading: false,
				})
			}
		})
	},

	bindblur(e) {
		this.setData({
			inputShowed: false,
		})
	},
	showInput() {
		this.setData({
			inputShowed: true
		});
	},
	hideInput() {
		this.setData({
			inputVal: "",
			inputShowed: false
		});
	},
	clearInput() {
		this.setData({
			inputVal: ""
		});
	},
	inputTyping(e) {
		this.setData({
			inputVal: e.detail.value
		});
	},
	search(e) {
		this.setData({
			inputVal: e.detail.value || '',
			inputShowed: false
		});
		wx.reLaunch({
			url: '/pages/tabbar/schoollist/schoollist?search=' + e.detail.value
		})
	},
	onShareAppMessage(res) {
		return {
			title: '考研',
			path: '/pages/tabbar/index/index'
		}
	},
	showDetail(e) {
		wx.navigateTo({
			url: '/pages/course-detail/course-detail?id=' + e.detail,
		})
	},
	navCoursePage(e) {
		wx.navigateTo({
			url: '/pages/course-detail/course-detail?id=' + e.currentTarget.dataset.id,
		})
	},
	navActlPage(e){
		wx.navigateTo({
			url: '/pages/act-detail/act-detail?id=' + e.currentTarget.dataset.id,
		})
	},
	navPorject(e) {
		wx.navigateTo({
			url: '/pages/porject-page/porject-page?id=' + e.currentTarget.dataset.id,
		})
	},
	addMore() {
		wx.showLoading({
			title: '加载中...',
		})
		self.setData({
			openState: true
		})
		
		let deadtime = wx.getStorageSync('ky_school_deadtime')  // 获取本地存储时间
		if (deadtime) {
			if (parseInt(deadtime) < Date.parse(new Date()) / 1000) {  // 本地存储失效
				removeStorage('ky_school')
				self.getSchoolList({ per_page: 100 })
			} else {
				setTimeout(function () {
					wx.hideLoading()
				}, 1000)
				self.setData({
					schoolLists: getStorageSync('ky_school'),
				})
			}
		} else {
			self.getSchoolList({ per_page: 100 })
		}
	},
	hiddenMore(){
		self.setData({
			openState: false
		})
	},
	onSwiperTap(e) {
		let url = e.target.dataset['url']
		let navType = e.target.dataset['navtype']
		if (navType == "tabbar") {  // 是否跳转 tabbar 页面
			wx.reLaunch({
				url,
			})
		} else {
			wx.navigateTo({
				url,
			})
		}

	},
})
