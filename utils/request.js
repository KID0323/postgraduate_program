const { BASEAPIURL } = require("../config/index.js");  // 数据请求基本地址
const errCodes = require('../config/errcodes.js');  // 部分错误代码对应的错误消息，用来自定义错误提示
const { parseJson } = require("./util.js");
// 数据请求
const wxRequest = (apiUrl, params = {}, method) => {
	return new Promise((resolve, reject) => {
		wx.request({
			url: `${BASEAPIURL}${apiUrl}`,
			method: method || 'GET',  // 请求方式，默认get
			data: params.data || {},  // 数据请求内容
			dataType: 'json',  // 数据格式json
			header: {
				Accept: 'application/json',
				"Content-Type": params.ContentType || "application/json",
				"Authorization": params.noAuth ? "" : "Basic bHp6Omx1emhlbnpoZW4="
			},
			success(res) {
				let data = parseJson(res.data);
				if (data.errCode == 200) {
					typeof params.success == "function" && params.success(data, res.header);
					resolve(data, res.header);
				} else {  // 服务端返回错误，error函数可以自定义错误处理，error函数返回true,不提示错误消息
					if (typeof params.error == "function") {
						let hideError = params.error(data);
						if (!hideError) {
							setTimeout(() => {
								wx.showToast({
									title: data.errMsg ? errCodes[data.errCode] : errCodes[res.statusCode] || `${res.statusCode}` ,
									image: '/assert/imgs/error_tip.png',
									duration: 2000,
								});
							}, 0)
						}
					} else {
						setTimeout(() => {
							wx.showToast({
								title: data.errMsg ? errCodes[data.errCode] : errCodes[res.statusCode] || `${res.statusCode}` ,
								image: '/assert/imgs/error_tip.png',
								duration: 2000,
							});
						}, 0)
					}
					// typeof params.error == "function" && params.error(data);
					reject(res);
				}
			},
			fail(res) {  // 网络错误
				setTimeout(() => {
					wx.showModal({
						title: '请求失败！',
						content: '请检查网络后，重新请求!',
					});
				}, 0)
				typeof params.fail == "function" && params.fail(res);
			},
			complete(res) {
				if (!params.showLoading) {  // showLoading：请求结束是否关闭加载状态
					setTimeout(function () {
						wx.hideLoading()
					}, 2000)
				}
				typeof params.complete == "function" && params.complete(res);
			},
		});
	})
};

module.exports = {
	wxRequest
}
