let dtime = '_deadtime';

const setStorageSync = (key, params, t) => {
	wx.setStorageSync(key, params)
	let seconds = parseInt(t);
	if (seconds > 0) {
		let timestamp = Date.parse(new Date());
		timestamp = timestamp / 1000 + seconds;
		wx.setStorageSync(key + dtime, timestamp + "")
	} else {
		wx.setStorageSync(key + dtime)
	}
}

const getStorageSync = (key, def) => {
	let deadtime = parseInt(wx.getStorageSync(key + dtime))
	if(deadtime){
		if(parseInt(deadtime) < Date.parse(new Date()) / 1000){
			if(def){
				return def;
			} else {
				return ;
			}
		}
	}
	let res = wx.getStorageSync(key);
	if(res) {
		return res;
	} else {
		return def;
	}
}

const removeStorage = (key) => {
	wx.removeStorageSync(key);
	wx.removeStorageSync(key + dtime);
}

const clearStorageSync = (key) => {
	wx.clearStorageSync();
}

module.exports = {
	setStorageSync,  // 设置缓存
	getStorageSync,  // 读取缓存
	removeStorage,  // 移除某个key
	clearStorageSync,  // 清空所有key
}